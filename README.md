### MIPS Programming Readme

This readme provides an overview of the solutions for the MIPS programming tasks outlined in the specifications.

---

#### Task 1: Decision Making and Conditionals
- **Aim:** Translate Python code to MIPS involving decisions and conditions.
- **Description:** Implement a MIPS program that reads input numbers and determines the number of divisors.
- **Constraints:** 
  - Proper handling of input and conditional statements.
  - Ensure correct output based on provided test cases.

#### Task 2: Loops and Array Access
- **Aim:** Translate Python code to MIPS involving loops and array access.
- **Description:** Develop a MIPS program to count multiples of a given number in a list.
- **Constraints:** 
  - Proper handling of array access and loop structures.
  - Ensure correct output based on provided test cases.

#### Task 3: Functions and Function Calls
- **Aim:** Define and call functions in MIPS, building on previous tasks.
- **Description:** Translate a functional version of Task 2's code into MIPS and call the function.
- **Constraints:** 
  - Utilize proper function definitions and function calls.
  - Ensure correct output based on provided test cases.

#### Task 4: Sorting Algorithms
- **Aim:** Implement an insertion sort algorithm in MIPS.
- **Description:** Translate Python code for an insertion sort function into MIPS assembly.
- **Constraints:** 
  - Properly document the function and provide line-by-line comments.
  - Ensure correct sorting of input arrays based on provided test cases.

#### Task 5: Recursive Functions
- **Aim:** Translate recursive function from Python to MIPS.
- **Description:** Translate a binary search function from Python to MIPS and call it with test inputs.
- **Constraints:** 
  - Properly document the function and provide line-by-line comments.
  - Ensure correct search results based on provided test cases.

---
