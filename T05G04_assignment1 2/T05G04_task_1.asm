# Author: Anushka Reddy 
# Global variables: number, second_divisor, divisors, prompt, first_divisor_prompt, second_divisor_prompt, final_prompt, newline
# A program to calculate how many of the two divisors enetered, will divide the number entered, resulting in no remainder

#The global variables used in the program are stored here
.data 
	number: .word 0
	first_divisor: .word 0
	second_divisor: .word 0
	divisors: .word 0 
	prompt: .asciiz "Enter the number: "
	first_divisor_prompt: .asciiz "Enter the first divisor: "
	second_divisor_prompt: .asciiz "Enter the second divisor: "
	final_prompt: .asciiz "Divisors: "
	newline: .asciiz "\n"

#.Intructions begin here	
.text 
	#Loads prompt to user
	la $a0, prompt 
    	addi $v0, $0, 4 
    	syscall
    	
    	#enables the user to input a number
    	addi $v0, $0, 5 
    	syscall
    	sw $v0, number 
	
	#Loads first_divisor_prompt to user
    	la $a0, first_divisor_prompt 
    	addi $v0, $0, 4 
    	syscall
    	
    	#enables user to input their first divisor
    	addi $v0, $0, 5 
    	syscall
    	sw $v0, first_divisor

	
	#Loads second_divisor_prompt to user
    	la $a0, second_divisor_prompt
    	addi $v0, $0, 4
    	syscall
    	
    	#enables user to input their second divisor
    	addi $v0, $0, 5 
    	syscall
    	sw $v0, second_divisor 
    	
    	
    	#perform operations
    	
    	#check if the first divisor will divide number with no remainder
    	lw $t0, number
    	lw $t1, first_divisor 
    	div $t0, $t1
    	#the content of hi will contain the remainder of the division
    	mfhi $t3 #move contents from hi to $t3
    	bne $zero, $t3, check_next_divisor #contents of t3 !=0?
    	#if the remainder is 0, the program will continue to check the first if condition 
    	#if the remainder is not zero, program will go to the elif condition
    	
    	#check if the second divisor will also divide the number with no remainder
    	lw $t2, second_divisor
    	div $t0, $t2
    	#the content of hi will contain the remainder of the division
    	mfhi $t4 #move contents from hi to $t4
    	bne $zero, $t4, print_one #contents of t4!=0?
    	#if the remainder is not zero, program will go to print_one, since only the first_divisor was true
    	#if the remainder is 0, this means both the first and second divisor are true, so the program needs to print two
    	
    	#to display the that there are two divisors 
    	lw $t7, divisors
    	addi $t7, $t7, 2
    	sw $t7, divisors
    	la $a0, final_prompt
   	addi $v0, $0, 4
   	syscall
   	lw $a0, divisors
    	addi $v0, $0, 1
    	syscall 
    	#next we need to display a newline 
    	j print_newline
    	
    	#to check the second divisor, in the case where the first divisor was not True
    	check_next_divisor:
    		lw $t2, second_divisor
    		div $t0, $t2
    		# the remainder will be stored in hi: 
    		mfhi $t5 #move contents from hi to $t5	
    		bne $zero, $t5, print_zero #contents of t5!=0?
    		#if the remainder is not equal to zero, that means neither divisor is True, so we need to go to the else condition
    		# if the remainder is 0, this means we need to print one, since this satisfies the elif condition
    		
    	
    	# to display that there is one divisor
    	print_one:
    	    	lw $t7, divisors
    		addi $t7, $t7, 1
    		sw $t7, divisors
    		la $a0, final_prompt
   		addi $v0, $0, 4
   		syscall
   		lw $a0, divisors
    		addi $v0, $0, 1
    		syscall 
    		j print_newline
    	
    	# to display that there are no divisors
    	print_zero:
    	    	la $a0, final_prompt
   		addi $v0, $0, 4
   		syscall
    		lw $a0, divisors
    		addi $v0, $0, 1
    		syscall 
    		
    	# to print a newline
    	print_newline:
    		la $a0, newline
    		addi $v0, $0, 4
    		syscall
    		
    	# to exit the program
    	addi $v0, $0, 10 
       	syscall


    		
    		
    	
    		
    		
    		
