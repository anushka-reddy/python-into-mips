# Author: Anushka Reddy 
# Global variables: output1, output2, newline
# A program to iterate through an array and check if each item in the array is a multiple of n, the program then returns the count of the number of multiples


# the global variables are stored here
.data
output1: .asciiz "The number of multiples of "
output2: .asciiz " is: "
newline: .asciiz "\n"

#the instruction set begins here
.text 

.globl main
.globl get_multiples

#stack diagram for main:
# n is at -8($fp)
# address of array is at -4($fp)

#stack diagram for get_multiples:
# i is at -8($fp)
# count is at -4($fp)
# fp is at ($fp)
# ra is at 4($fp)
# address of array is at 8($fp)
# n is at 12($fp)
  

main:

	#copy the sp into fp
	addi $fp, $sp, 0
	#make space for locals, in this case the address of the array and n
	addi $sp, $sp, -8
	
	#initialize my_list 
	
	#start by allocating bytes 
	addi $a0, $0, 16 # (4*3)+4 
	addi $v0, $0, 9 
	syscall
	
	#store the address of the array into the stack
	sw $v0, -4($fp)
	
	#store size of array at first element in array
	lw $t1, -4($fp) #loading array address from stack
	addi $t0, $0, 3
	sw $t0, ($t1) #store 3 as first element in array
	
	#store the elements into the array
	lw $t1, -4($fp) 
	addi $t0, $0, 2
	sw $t0, 4($t1)
	addi $t0, $0, 4
	sw $t0, 8($t1)
	addi $t0, $0, 6
	sw $t0, 12($t1)	
	
	#store n into the stack
	addi $t4, $0, 3
	sw $t4, -8($fp)
	
	#store the arguments of get_multiples onto stack
	addi $sp, $sp, -8
	lw $t0, -4($fp) 
	sw $t0, 0($sp)#storing the address of array
	lw $t0, -8($fp)
	sw $t0, 4($sp) #storing n as an argument
	
	
	#call get_multiples
	jal get_multiples
	
	#after execution of get_multiples 
	#remove the arguments
	addi $sp, $sp, 8
	# Store return value
	addi $sp, $sp, -4 #make space to store the count
	sw $v0, -12($fp) #store count here
	
	# Print result
	#first prompt
	la $a0, output1
	addi $v0, $0, 4
	syscall
	
	#print n
	lw $a0, -8($fp)
	addi $v0, $0, 1
	syscall
	
	#print second prompt
	la $a0, output2
	addi $v0, $0, 4
	syscall
	
	#print count
	lw $a0, -12($fp)
	addi $v0, $0, 1
	syscall
	
	# to print a newline
    	la $a0, newline
    	addi $v0, $0, 4
    	syscall
	
	#remove locals, then exit
	addi $sp, $sp, 12
	addi $v0, $0, 10
	syscall
	
	
get_multiples: 
	# Save $ra and $fp in stack
	addi $sp, $sp, -8 
	sw $ra, 4($sp) 
	sw $fp, 0($sp) 
	
	#copy sp to fp
	addi $fp, $sp, 0
	
	#initialize the locals 
	addi $sp, $sp, -8
	addi $t5, $0, 0 
	sw $t5, -4($fp)#count is here
	addi $t6, $0, 0 
	sw $t5, -8($fp) #i is here
	
loop: 	
	#check that i<size of list 
	lw $t0, -8($fp) #i
	lw $t1, 8($fp) #load addr of array
	lw $t2, ($t1) #load first elem of array, in this case size of the array
	slt $t0, $t0, $t2 #check that i<size of list 
	beq $t0, $0, exit_get_multiples #if size of lst=>i, go to return
	
	#get the addr to point to next element in array  
	lw $t0, 8($fp) #addr in t0
	lw $t2, -8($fp) #loading i into t2
	#add i*4 to address
	sll $t1, $t2, 2 #i*4
	add $t0, $t0, $t1 #i*4+addr
	#check lst[i]%n=0
	lw $t2, 12($fp)	#load n
	lw $t1, 4($t0) #lst[i]
	div $t1, $t2
	mfhi $t3
	bne $t3, $0, increment_i #if not true go to increment_i
	
	#if true, check second condition lst[i]!=n
	lw $t0, 8($fp) #addr in t0
	lw $t2, -8($fp) #loading i into t2
	#add i*4 to address
	sll $t1, $t2, 2 #i*4
	add $t0, $t0, $t1 #i*4+addr
	lw $t1, 4($t0) #lst[i]
	lw $t2, 12($fp)	#load n
	
	#if equal go to increment_i
	beq $t1, $t2, increment_i #lst[i]==n?
	
	#if not equal increment count
	lw $t0, -4($fp)
	addi $t0, $t0, 1
	sw $t0, -4($fp)
	
increment_i:
	#i+=1
	lw $t0, -8($fp)
	addi $t0, $t0, 1
	sw $t0, -8($fp)
	j loop 
	

exit_get_multiples:
	#put count into $v0
	lw $v0, -4($fp)
	addi $sp, $sp, 8
	# Restore $fp and $ra
	lw $fp, 0($sp) #restore $fp
	lw $ra, 4($sp) #restore $ra
	addi $sp, $sp, 8 #deallocate
	
	# Return to caller
	jr $ra
	
	
	
	
	
	
	
	
	
	
	
	
	
