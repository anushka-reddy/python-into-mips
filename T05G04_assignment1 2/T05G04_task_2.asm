#Author: Nabiha Siddiqui, Anushka Reddy	
#Global variables: sizeInput, nInput, size, n, count, list, i, listInput, output, newline
#Description: A program to iterate through an array and check if each item in the array is a multiple of n, the program then prints the count of the number of multiples. We are assuming for the sake of this assignment that 0 is a multiple of any number.

.data 
sizeInput: .asciiz "Enter array length: "
nInput: .asciiz "Enter n: "
size: .word 0
n: .word 0
count: .word 0 
list: .word 0
i: .word 0
listInput: .asciiz "Enter the value: "
output: .asciiz "The number of multiples (excluding itself) = "
newline: .asciiz "\n"

.text 

#get user input for array length, assuming the size of the list will always be positive
la $a0, sizeInput #display prompt
addi $v0, $0, 4
syscall

#get user input for size
addi $v0, $0, 5
syscall
sw $v0, size #store input

#allocate bytes for size of array 
addi $v0, $0, 9
lw $a0, size
sll $a0, $a0, 2 
addi $a0, $a0, 4 #4 extra bytes to store the size of array
syscall

#store address of array into list
sw $v0, list

#store size of array at first element in array
lw $t0, size
lw $t1, list #load addr
sw $t0, ($t1)

#get user input for n, assuming the n value will always be a positive integer
la $a0, nInput #display prompt
addi $v0, $0, 4
syscall

#get user input for size
addi $v0, $0, 5 
syscall
sw $v0, n #store input


#loop to get user inputs for the value and check against conditions
loop: 	
	#check that i<size of list 
	lw $t0, i #i
	lw $t1, list #load addr of array
	lw $t2, ($t1) #load first elem of array, in this case size of the array
	slt $t0, $t0, $t2 #check that i<size of list 
	beq $t0, $0, printRes #if size of lst=>i, go to print
	
	#get the addr to point to next element in array  
	lw $t0, list #addr in t0
	lw $t2, i #loading i into t2
	#add i*4 to address
	sll $t1, $t2, 2 #i*4
	add $t0, $t0, $t1 #i*4+addr in t0
	
	#get user input for value
	la $a0, listInput #display prompt
	addi $v0, $0, 4
	syscall
	addi $v0, $0, 5
	syscall
	sw $v0, 4($t0) #store input into list
	
	#check lst[i]%n=0
	lw $t2, n	#load n
	lw $t1, 4($t0) #load lst[i]
	div $t1, $t2
	mfhi $t3
	bne $t3, $0, increment_i #if not true go to increment_i
	
	#if true, check second condition lst[i]!=n
	lw $t0, list
	lw $t2, i #loading i into t2
	#add i*4 to address
	sll $t1, $t2, 2 #i*4
	add $t0, $t0, $t1 #i*4+addr
	lw $t1, 4($t0) #lst[i]
	lw $t2, n	#load n
	
	#if equal go to increment_i
	beq $t1, $t2, increment_i
	
	#if not equal increment count
	lw $t0, count
	addi $t0, $t0, 1
	sw $t0, count
	
increment_i:
	#using the formula: i+=1
	lw $t0, i
	addi $t0, $t0, 1
	sw $t0, i
	j loop 

printRes:
	#print result
	#first prompt
	la $a0, output
	addi $v0, $0, 4
	syscall
	
	#print count
	lw $a0, count
	addi $v0, $0, 1
	syscall
	
	#to print a newline
    	la $a0, newline
    	addi $v0, $0, 4
    	syscall

	#exit
	addi $v0, $0, 10
	syscall
