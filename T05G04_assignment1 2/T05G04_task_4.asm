#Author: Aiguo Shao, Yuheng Shang, Anushka Reddy 	
#Global variables: output (which is a empty space), newline	
#Description: a main function that calls an insertion sort function, to order the list in a non-decreasing order.	
	
	.data
output: .asciiz " "
newline: .asciiz "\n"

	.text
	
	.globl main
	.globl insertion_sort
main:   

#stack frame diagram for main:

# i is at -8($fp)
# addr of array is at -4($fp)	
			
					
#stack frame diagram for insertion_sort:

# key is at -12($fp)
# i is at -8($fp)
# j is at -4($fp)
# fp is at ($fp)	
# ra is at 4($fp)			
# addr of array is at 8($fp)	

		
	#copy $sp into $fp
	addi $fp, $sp, 0
	#allocate 24 bytes for, 4 from it is for lenth
	addi $sp, $sp, -4
	
	#start by allocate the bytes
	addi $v0, $0, 9
	addi $a0, $0, 24 # (5 * 4) + 4 = 24	
	syscall
	
	#store the address of the array into the stack
	sw $v0, -4($fp)
	
	#store size of the array at the first element in array
	lw $t1, -4($fp) #load the array
	addi $t0, $0, 5
	sw $t0, ($t1) #store the length into the first element
	
	#store the elements of the array
	lw $t1, -4($fp)
	
	addi $t7, $0, 6
	sw $t7, 4($t1) #the first element is 6
	
	addi $t2, $0, -2
	sw $t2, 8($t1) #the second element is -2
	
	addi $t3, $0, 7
	sw $t3, 12($t1) #the third element is 7
	
	addi $t4, $0, 4
	sw $t4, 16($t1) #the fourth element is 4
	
	addi $t5, $0, -10
	sw $t5, 20($t1) #the fifth element is -10
	
	#call insertion_sort(the_list:List[T])
	#push 4 bytes (include length)
	#of arguments
	addi $sp, $sp, -4
	lw $t0, -4($fp)
	sw $t0, 0($sp)
	
	#link and goto insertion_sort
	jal insertion_sort

	#store array value
	lw $t0, 0($sp)
	sw $t0, -4($fp)
	
	#remove arguments
	addi $sp, $sp, 4
	
	#add local i
	addi $sp, $sp, 4
	addi $t6, $0, 0
	sw $t6, 0($sp)
	
print_array:	
	lw $t0, 0($sp)#load i
	lw $t1, -4($fp) #load the address of array
	lw $t2, 0($t1) #load the length
	slt $t0, $t0, $t2 #is i < length?
	beq $t0, $0, exit #if i >= length, go back to $ra
	
	lw $t2, -4($fp) #load the array
	lw $t0, 0($sp) #load i
	sll $t0, $t0, 2  #i * 4
	add $t0, $t0, $t2
	addi $v0, $0, 1
	lw $a0, 4($t0) #t2 = the_list[i]
	syscall
	
	#print the space
	la $a0, output
	addi $v0, $0, 4
	syscall
	
	#i += 1
	lw $t0, 0($sp)#load i
	addi $t0, $t0, 1 # i+1
	sw $t0, 0($sp) # i = i+1
	
	j print_array
	
	
exit:	# to print a newline
    	la $a0, newline
    	addi $v0, $0, 4
    	syscall
    	
	#remove locals, then exit

	addi $sp, $sp, 8
	addi $v0, $0, 10
	syscall
	
	
		
insertion_sort:

	#save $ra and $fp in stack
	addi $sp, $sp, -8 #make space
	sw $ra, 4($sp) 	#save $ra
	sw $fp, 0($sp)  #save $fp
	
	#copy $sp to $fp
	addi $fp, $sp, 0
	
	#allocate local	
	#3 * 4 = 8 bytes. (key, i and j)
	addi $sp, $sp, -12
	
	#initialize locals
	addi $t0, $0, 0
	sw $t0, -12($fp) # key = 0
	
	addi $t0, $0, 1
	sw $t0, -8($fp) # i =1 (i start from 0)
	
	addi $t0, $0, 0
	sw $t0, -4($fp) # j =0	
	
	
forloop:
	#keep going if i < length
	lw $t0, -8($fp) #load i
	lw $t1, 8($fp) #load array
	lw $t2, ($t1) #load the length of the array
	slt $t0, $t0, $t2 #is i < length?
	beq $t0, 0, exit_insertion_sort # if not go to end
	
	#key = the_list[i]
	lw $t2, 8($fp) #load the array
	lw $t0, -8($fp) #load i
	addi $t1, $0, 4 #let $t1=4
	mult $t0,$t1 #i * 4 
	mflo $t0  #store the result into $t0
	add $t2, $t2, $t0
	lw $t1, 4($t2) #t1 = the_list[i]
	sw $t1, 0($sp) #key = the_list[i]
	
	#j = i - 1
	lw $t0, -8($fp) #load i
	addi $t0, $t0, -1  # i - 1
	sw $t0, -4($fp) #j = i - 1
	
	whileloop:
	#j>=0
	lw $t0, -4($fp) #load j
	addi $t7, $0, 0
	slt $t0, $t0, $t7 #is j < 0?
	bne $t0, 0, end_for
	 
	#and  key < the_list[j]
	#the_list[j]
	lw $t2, 8($fp) #load the array
	lw $t0, -4($fp) #load j
	addi $t5, $0, 4 #let $t5=4
	mult $t0,$t5 #j * 4 
	mflo $t0  #store the result into $t0
	add $t2, $t2, $t0
	lw $t1, 4($t2) #t1 = the_list[j]
	
	lw $t2, -12($fp) #load key
	slt $t3, $t2, $t1 #is key < the_list[j]?
	beq $t3, $0, end_for #if not less than the_list[j], go to for loop.
	
	#the_list[j+1]
	lw $t0, 8($fp) #load array
	lw $t2, -4($fp) #load j
	addi $t2, $t2, 1 #j+1
	addi $t6, $0, 4 #t6 = 4
	mult $t2, $t6 #(j + 1) * 4
	mflo $t2  #t2 = (j + 1) * 4
	add $t0, $t0, $t2 # &(the_list[j+1])-4
	sw $t1, 4($t0) #t1=the_list[j+1]
	
	#j-= 1
	lw $t0, -4($fp) #load j
	addi $t0, $t0, -1 # j-1
	sw $t0, -4($fp) #j = j - 1
	
	j whileloop
	
end_for:	
	#the_list[j+1] = key
	lw $t0, 8($fp) #load array
	lw $t2, -4($fp) #load j
	addi $t2, $t2, 1 #j+1
	addi $t6, $0, 4 #t6 = 4
	mult $t2, $t6 #(j + 1) * 4
	mflo $t2  #t2 = (j + 1) * 4
	add $t0, $t0, $t2 # &(the_list[j+1])-4
	lw $t7, -12($fp) #load key
	sw $t7, 4($t0) #key = the_list[j+1]
	
	#i += 1
	lw $t0, -8($fp)
	addi $t0, $t0, 1 #i+1
	sw $t0, -8($fp) #i = i + 1
	
	j forloop
	
	
exit_insertion_sort:	
	#remove local var
	addi $sp, $sp, 12
	#restore $fp and $ra
	lw $fp, 0($sp) 
	lw $ra, 4($sp)
	addi $sp, $sp, 8 #deallocate
	
	#return to caller
	jr $ra
