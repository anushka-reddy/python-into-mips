#Author: Aiguo Shao, Anushka Reddy
#Global variables: newline
#Description: a main function that calls a function binary search, which recursively calls itself, to find the index of the target value in a list.
	
	.data
newline: .asciiz "\n"
	
	.text
		
	.globl main
	.globl binary_search
														
													

main:
#stack frame diagram for main here:

#index is at -8($fp)	
# address of array is at -4($fp)		


#stack frame diagram for binary_search here:

# mid is at -4($fp)
# fp is at ($fp)
# ra is at 4($fp)			
# address of array is at 8($fp)
# target is at 12($fp)
# low is at 16($fp)
# high is at 20($fp)				
	
	#copy $sp into $fp
	addi $fp, $sp, 0
	#allocate 4 bytes for array
	addi $sp, $sp, -4
	
	#start by allocate the bytes
	addi $v0, $0, 9
	addi $a0, $0, 24 # (5 * 4) + 4 = 24	
	syscall
	
	#store the address of the array into the stack
	sw $v0, -4($fp)
	
	#store size of the array at the first element in array
	lw $t1, -4($fp) #load the array
	addi $t0, $0, 5
	sw $t0, ($t1) #store the length into the first element
	
	#store the elements of the array
	lw $t1, -4($fp)
	
	addi $t2, $0, 1
	sw $t2, 4($t1) #the first element is 1
	
	addi $t3, $0, 5
	sw $t3, 8($t1) #the second element is 5
	
	addi $t4, $0, 10
	sw $t4, 12($t1) #the thirth element is 10
	
	addi $t5, $0, 11
	sw $t5, 16($t1) #the fourth element is 11
	
	addi $t6, $0, 12
	sw $t6, 20($t1) #the fivth element is 12
	
	#call binary_search(the_list: list, target: int, low: int, high: int)
	#push 16 bytes for four arguements
	addi $sp, $sp, -16
	lw $t0, -4($fp)
	sw $t0, 0($sp)#arguement1 = array
	
	addi $t5, $0, 11 #t5  = 11
	sw $t5, 4($sp) #arguement2 = target (11)
	
	addi $t5, $0, 0 #t5 = 0
	sw $t5, 8($sp) #arguement3 = low (0)
	
	lw $t0, -4($fp) #load array
	lw $t1, ($t0) #load the length of the arr
	addi $t1, $t1, -1 # t1= len(arr) - 1
	sw $t1, 12($sp) #arguement4 = high (len(arr) - 1)
	
	#link and goto binary_search
	jal binary_search
	
	# Clear argument.
	addi $sp, $sp, 16
			
	#add a new local index
	addi $sp, $sp, -4
	sw $v0, -8($fp) #index = binary_search(arr, 11, 0, len(arr) - 1)
	lw $t1, -8($fp) #load index
	addi $a0, $t1, 0 #index
	addi $v0, $0, 1
	syscall
	# to print a newline
    	la $a0, newline
    	addi $v0, $0, 4
    	syscall

	# remove locals, then exit
	addi $sp, $sp, 8
	addi $v0, $0, 10 
	syscall
	
	
	
binary_search:
	
	# Save $ra and $fp in stack
	addi $sp, $sp, -8 # make space
	sw $ra, 4($sp) # save $ra
	sw $fp, 0($sp) # save $fp	
	
	# Copy $sp to $fp
	addi $fp, $sp, 0
	
	# Alloc local variables
	# 1 * 4 = 4 bytes.
	addi $sp, $sp, -4
	
	# Initialize locals.
	addi $t0, $0, 0
	sw $t0, -4($fp) # mid=0
	
	#if 
	lw $t0, 16($fp) #t0=low
	lw $t1, 20($fp) #t1=high
	slt $t0, $t1, $t0 #is high < low ?
	beq $t0, $0, else # if not , go to else
	
	#return -1
	addi $v0, $0, -1 # $v0 = -1
	
	# Remove local var.
	addi $sp, $sp, 4
	
	# Restore $fp and $ra
	lw $fp, 0($sp) #restore $fp
	lw $ra, 4($sp) #restore $ra
	addi $sp, $sp, 8 #deallocate
	
	# Return to caller.
	jr $ra
	
	
else:	
	#mid = (high + low) // 2
	lw $t0, 16($fp) #load low
	lw $t1, 20($fp) #load high
	add $t0, $t0, $t1 #t0= low+high
	sra $t2, $t0, 1 #t2= (high + low) // 2
	sw $t2, -4($fp) #mid=t2
	
	#if the_list[mid] == target
	lw $t0, 8($fp) #load the array
	lw $t1, -4($fp) #load mid
	addi $t2, $0, 4 #let $t2=4
	mult $t1,$t2 #mid * 4 
	mflo $t1  #store the result into $t1
	add $t0, $t0, $t1
	lw $t3, 4($t0) #t3 = the_list[mid]
	lw $t4, 12($fp)#load target
	bne $t3, $t4, elif
	
	#return mid in $v0
	lw $v0, -4($fp) #$v0=mid
	
	# Remove local var.
	addi $sp, $sp, 4
	
	# Restore $fp and $ra
	lw $fp, 0($sp) #restore $fp
	lw $ra, 4($sp) #restore $ra
	addi $sp, $sp, 8 #deallocate
	
	# Return to caller.
	jr $ra
	
elif:	
	#if the_list[mid] > target:
	lw $t0, 8($fp) #load the array
	lw $t1, -4($fp) #load mid
	addi $t2, $0, 4 #let $t2=4
	mult $t1,$t2 #mid * 4 
	mflo $t1  #store the result into $t1
	add $t0, $t0, $t1
	lw $t3, 4($t0) #t3 = the_list[mid]
	lw $t4, 12($fp)#load target
	slt $t1, $t4, $t3#is target < the_list[mid]?
	beq $t1, $0, else2 #if not, go to else2
	
	#return binary_search(the_list, target, low, mid - 1)
	# Recursive call.
	# 4 * 4 = 24 bytes arg
	addi $sp, $sp, -24 #alloc space
	# argument 1 = the_list(array)
	lw $t0, 8($fp) # load array
	sw $t0, 0($sp) # arg 1=the_list(array)
	
	# argument 2 = target
	lw $t0, 12($fp) # load target
	sw $t0, 4($sp) # arg 2 = target
	
	# argument 3 = low
	lw $t0, 16($fp) # load low
	sw $t0, 8($sp) # arg 3 = low
	
	# argument 4 = mid - 1
	lw $t0, -4($fp) # load mid
	addi $t0, $t0, -1#mid - 1
	sw $t0, 12($sp) # arg 4 = mid - 1
	
	# call binary_search
	jal binary_search
	# Deallocate argument
	addi $sp, $sp, 24
	
	#return binary_search(the_list, target, low, mid - 1)
	addi $v0, $v0, 0
	# Deallocate local variable
	addi $sp, $sp, 4
	# Restore $fp and $ra
	lw $fp, 0($sp) # restore $fp
	lw $ra, 4($sp) # restore $ra
	addi $sp, $sp, 8 # dealloc
	# return
	jr $ra 


else2:  #if the_list[mid] < target:
	#return binary_search(the_list, target, mid + 1, high)
	# Recursive call.
	# 4 * 4 = 24 bytes arg
	addi $sp, $sp, -24 #alloc space
	# argument 1 = the_list(array)
	lw $t0, 8($fp) # load array
	sw $t0, 0($sp) # arg 1=the_list(array)
	
	# argument 2 = target
	lw $t0, 12($fp) # load target
	sw $t0, 4($sp) # arg 2 = target
	
	# argument 3 = mid + 1
	lw $t0, -4($fp) # load mid
	addi $t0, $t0, 1 #mid+1
	sw $t0, 8($sp) # arg 3 = mid + 1
	
	# argument 4 = high
	lw $t0, 20($fp) # load high
	sw $t0, 12($sp) # arg 4 = high
	
	# call binary_search
	jal binary_search
	# Deallocate argument
	addi $sp, $sp, 24
	
	#return binary_search(the_list, target, low, mid - 1)
	addi $v0, $v0, 0
	# Deallocate local variable
	addi $sp, $sp, 4
	# Restore $fp and $ra
	lw $fp, 0($sp) # restore $fp
	lw $ra, 4($sp) # restore $ra
	addi $sp, $sp, 8 # dealloc
	# return
	jr $ra 
	
	
	
	
	
	
	
