#A program which will recursively call itself, in search of a target number and will return the index of the target number.
def binary_search(the_list: list, target: int, low: int, high: int) -> int:
    if low > high:    # if low> high, return the number -1
        return -1     #base case, if the index stored in low is more than the index stored in high, return -1 as the target is not in the list
    else:              #if low is not greater than high, keep going
        mid = (high + low) // 2      #let mid be the mid point of high+low 

        if the_list[mid] == target: # if the mid element in the_list is equal to target
            return mid             # return the value of mid
        elif the_list[mid] > target: # if the mid element in the_list is greater than target
            return binary_search(the_list, target, low, mid - 1)   #have a recursion with the same arguments as before, except high will be replaced with the middle element+1, so this time the program will check only the first half of the list
        else:
            return binary_search(the_list, target, mid + 1, high)# if the mid element in the_list is smaller than target
#have a recursion with the same arguments as before, except low will be replaced with the middle element of the list +1,so this time the program will only check the last half of the list.
def main() -> None:
    arr = [1, 5, 10, 11, 12]  #create a sorted list as input of the function
    index = binary_search(arr, 11, 0, len(arr) - 1)  #call the function with 4 arguements, and store the result in index
    print(index) #print the target's index


main()
