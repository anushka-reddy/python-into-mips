# Add comments to this file
from typing import List, TypeVar

T = TypeVar('T')    #can be anything


def insertion_sort(the_list: List[T]):  #create a function of insertion sort.   
    length = len(the_list)     #let length = the number of elements in the_list
    for i in range(1, length): # a for loop, start from the second element
        key = the_list[i]      #using key to be  the each element in the list
        j = i-1             #set j is 1 less than i
        while j >= 0 and key < the_list[j]:# if the element is less than the previous
            the_list[j + 1] = the_list[j] #the element will equal the previous
            j -= 1        #j minus 1
        the_list[j + 1] = key  #the previous element will be the element after


def main() -> None:
    arr = [6, -2, 7, 4, -10]   #create a list contains 5 numbers.
    insertion_sort(arr)       #call the insertion_sort function
    for i in range(len(arr)):   #for i in each element of arr
        print(arr[i], end=" ")  # print all the element in arr
    print()


main()
